
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(file, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
    this.asynchReadTextFileResponse(ActorPathProject.getFile(file, projectType, isExternalPlugin, workspaceName));
  }
}


module.exports = CodeEditorFileGet;
