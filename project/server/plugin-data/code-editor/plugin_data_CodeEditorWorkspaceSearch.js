
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Os = require('os');
const Path = require('path');


class CodeEditorWorkspaceSearch extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(search) {
    this.expectAsynchResponseTemp();
    this.asynchReadFile(ActorPathGenerated.getWorkspaceRecentFiles(), (err, data) => {
      if(err) {
        this.expectAsynchResponseError(`Could not get recent project files '${ActorPathGenerated.getWorkspaceRecentFiles()}'.`);
        this.unExpectAsynchResponseTemp();
      }
      else {
        const lastWorkspace = data.recentWorkspaces[0];
        this.asynchReadFile(ActorPathProject.getWorkspaceFile(lastWorkspace.appName, lastWorkspace.workspaceName), (err, workspace) => {
          if(!err) {
            let pendings = 0;
            const fileDatas = [];
            const searchedData = {
              hits: 0,
              files: 0,
              searched: 0
            };
            workspace.projects.push({
              project: '',
              type: ''
            });
            for(let i = 0; i < workspace.projects.length; ++i) {
              const project = workspace.projects[i];
              ++pendings;
              this._searchFiles(fileDatas, searchedData, ActorPathProject.getCodeProjectFolder(project.name, project.type), project.name ? `${project.name}${Path.sep}${project.type}` : 'project', (err) => {
                if(0 === --pendings) {
                  let inPendings = fileDatas.length;
                  const foundFiles = [];
                  fileDatas.forEach((fileData) => {
                    this._searchInFiles(foundFiles, searchedData, fileData, search, () => {
                      if(0 === --inPendings) {
                        this.expectAsynchResponseSuccess({
                          foundFiles: foundFiles,
                          hits: searchedData.hits,
                          files: searchedData.files,
                          searched: searchedData.searched
                        });
                        foundFiles.sort((a, b) => {
                          if(a.file < b.file) {
                            return -1;
                          }
                          else if(a.file > b.file) {
                            return 1;
                          }
                          else {
                            return 0;
                          }
                        });
                        this.unExpectAsynchResponseTemp();
                      }
                    });
                  });
                }
              });
            }
          }
        });
      }
    });
  }
  
  _searchFiles(fileNames, searchedData, searchPath, projectPath, done) {
    Fs.readdir(searchPath, (err, files) => {
      if(err) {
        return done(err);
      }
      if(undefined === files || 0 === files.length) {
        return done();
      }
      const fileData = {
        path: searchPath,
        projectPath: projectPath,
        files: []
      };
      let pendings = files.length;
      const filesLength = files.length;
      for(let i = 0; i < filesLength; ++i) {
        const path = searchPath + Path.sep + files[i];
        const pPath = projectPath + '/' + files[i];
        Fs.lstat(path, (err, stat) => {
          if(err) {
            ddb.error('Could not get stat from file, Error:', err);
            return;
          }
          if(stat.isDirectory()) {
            this._searchFiles(fileNames, searchedData, path, pPath, (err) => {
              if(0 === --pendings) {
                if(0 !== fileData.files.length) {
                  fileNames.push(fileData);
                }
                done();
              }
            });
          }
          else if(stat.isFile()) {
            ++searchedData.searched;
            fileData.files.push(files[i]);
            if(0 === --pendings) {
              if(0 !== fileData.files.length) {
                fileNames.push(fileData);
              }
              done();
            }
          }
        });
      }
    });
  }
  
  _searchInFiles(foundFiles, searchedData, fileData, search, done) {
    const filesLength = fileData.files.length;
    const files = fileData.files;
    const path = fileData.path;
    let pendings = filesLength;
    for(let i = 0; i < filesLength; ++i) {
      const file = path + Path.sep + files[i];
      const searchLength = search.length;
      Fs.readFile(file, (err, data) => {
        let index = 0;
        const found = {
          file: fileData.projectPath + '/' + files[i],
          indexes: []
        };
        while(-1 !== index) {
          index = data.indexOf(search, index);
          if(-1 !== index) {
            ++searchedData.hits;
            const currentIndex = index;
            index += searchLength;
            let startLineIndex = data.lastIndexOf(Os.EOL, currentIndex);
            if(-1 === startLineIndex) {
              startLineIndex = 0;
            }
            else {
              startLineIndex += Os.EOL.length;
            }
            let stopLineIndex = data.indexOf(Os.EOL, index);
            if(-1 === stopLineIndex) {
              stopLineIndex = data.length - 1;
            }
            found.indexes.push({
              index: index,
              lineIndex: currentIndex - startLineIndex,
              lineNbr: -1,
              lineBefore: data.slice(startLineIndex, currentIndex).toString(),
              lineAfter: data.slice(index, stopLineIndex).toString()
            });
          }
        }
        if(0 !== found.indexes.length) {
          ++searchedData.files;
          foundFiles.push(found);
          let index = 0;
          const lineIndexes = [];
          while(-1 !== index) {
            index = data.indexOf(Os.EOL, index);
            if(-1 !== index) {
              lineIndexes.push(index);
              index += Os.EOL.length;
            }
          }
          const indexes = found.indexes;
          const indexesLength = found.indexes.length;
          let currentLineIndex = 0;
          for(let i = 0; i < indexesLength; ++i) {
            for(;;) {
              if(lineIndexes[currentLineIndex] < indexes[i].index) {
                ++currentLineIndex;
                continue;
              }
              else {
                indexes[i].lineNbr = currentLineIndex + 1;
                break;
              }
            }
          }
        }
        if(0 === --pendings) {
          done();
        }
      });
    }
  }
}

module.exports = CodeEditorWorkspaceSearch;
