
'use strict';

const Project = require('z-abs-corelayer-cs/clientServer/project');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorProjectUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(content, name, type, isPlugin) {
    const isExternalPlugin = this.hasOrganization && !!isPlugin;
    const project = new Project(content);
    this.asynchWriteTextFile(ActorPathProject.getCodeProjectPureFile(name, type, isExternalPlugin), project.exportPure(), false);
    this.asynchWriteFileResponse(ActorPathGenerated.getProjectFile(name, type), content, false);
  }
}

module.exports = CodeEditorProjectUpdate;
