
'use strict';

const Os = require('os');


class TemplateHelpText {
  static create(lowercaseAppName, noDashAppName) {
    const text = `
${this.lowercaseAppName} usage: ${this.lowercaseAppName}|aj cmd [--[parameter name] [parameter value | parameter]] ...

cmd:

debug            Build and start ${noDashAppName} in debug mode.
release          Build and start ${noDashAppName} in release mode.
start            Start ${noDashAppName} in the mode it is built.
                   --httphost: the ${noDashAppName} HTTP host address. Default: 'localhost'.
                   --httpport: the ${noDashAppName} HTTP port. Default: 9005.
                   --wsp: the ${noDashAppName} websocket host address. Default: 'localhost'.
                   --wsh: the ${noDashAppName} websocket port. Default: 9006.
                   --bottleneck: limit the number of parallel processes building ${noDashAppName}. Default: [unlimited].
                   --build: just build, do not start ${noDashAppName}.
                   --buildall: build ${noDashAppName} and all other nodes.
                   --install: make an 'npm install' before other commands.
                   --test: start clients and servers with test config values if they exist.

clean:all        Removes the build, dist, temp, generated & node_modules folders.
clean:build      Removes the build folder. A new build has to be done with either '${lowercaseAppName} debug' or '${lowercaseAppName} release'.
clean:compiled   Removes the build and dist folders.
clean:dist       Removes the dist folder. A new build has to be done with either '${lowercaseAppName} debug' or '${lowercaseAppName} release'.
clean:generated  Removes the ../Generated folder.
clean:node       Removes the node_modules folder.
clean:temp       Removes the temp folder.

status           Show the changed files in the repos.
diff             Show the files diff in the repos.
clone            Clone repo
                   --repo: the name of the repo to clone.
                   --all: clone all repos that are not already cloned.
pull             Pull repo
                   --repo: the name of the repo to pull.
                   --all: pull all repos that are not already cloned.
tag              Tag repo.
                   --repo: the name of the repo to tag.
                   --all: tag all repos.
                   --name: the name of the tag.
                   --msg|message: the message of the tag.
    
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateHelpText;
