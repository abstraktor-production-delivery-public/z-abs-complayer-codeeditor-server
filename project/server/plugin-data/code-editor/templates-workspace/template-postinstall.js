
'use strict';

const Os = require('os');


class TemplatePostinstall {
  static create() {
const text = `
const GitSimple = require('simple-git');
const ChildProcess = require('child_process');
const Fs = require('fs');
const Path = require('path');

function zRepos(name, root) {
  const dirRoot = \`..\${Path.sep}\${root}\`;
  const dirRepo = \`\${dirRoot}\${Path.sep}\${name}\`;
  const repoUrl = \`https://gitlab.com/abstraktor-development/\${name}.git\`;
  isRepo(dirRepo, (err, isRepo) => {
    if(err) {
      if('ENOENT' === err.code || -2 === err.errno) {
        Fs.mkdir(dirRepo, {recursive: true}, (e) => {
          if(e && -4075 !== e.errno) {
            console.error(\`mkdir \${dirRepo}: \${e}\`);
          }
          else {
            cloneRepo(dirRoot, repoUrl);
          }
        });
      }
      else {
        console.error(\`isRepo: \${err}\`);
      }
    }
    else if(!isRepo) {
      cloneRepo(dirRoot, repoUrl);
    }
  });
}

function startRepos(name) {
  const dir = \`..\${Path.sep}\${name}\`;
  isRepo(dir, (err, isRepo) => {
    if(!err && isRepo) {
      installRepo(isRepo, dir);
    }
  });
}

startRepos('z-abs-project');
startRepos('z-abs-local-server');

zRepos('actorjs-content-global', 'Content');
zRepos('actorjs-data-global', 'Data');
zRepos('actorjs-documentation-bin', 'Documentation');
zRepos('actorjs-documentation-text', 'Documentation');

function cloneRepo(dir, repo) {
  console.log('git clone repo ' + repo);
  GitSimple(dir).clone(repo, (err, result) => {
    console.log('git clone repo ' + repo + (err ? ', err: ' + err.message : ' SUCCESS') + (result ? ', result: ' + result : ''));
  });
}

function isRepo(dir, handle) {
  Fs.lstat(dir, (err, stats) =>  {
    if(err) {
      handle && handle(err, false);
    }
    else {
      if(stats.isDirectory()) {
        GitSimple(dir).checkIsRepo((err, result) => {
          handle && handle(err, result);
        });
      }
      else {
        handle && handle(new Error('Not a directory.'));
      }
    }
  });
}

function installRepo(isRepo, dir) {
  if(isRepo) {
    console.log('npm install ' + dir);
    try {
      ChildProcess.exec('npm install', {cwd: dir}, (err) => {
        if(err) {
          console.log('npm install ' + dir, err);
        }
        else {
          console.log('npm install ' + dir + ' SUCCESS');
        }
      });
    } catch (err) {
      console.error('npm install:', dir, err);
      return;
    }
  }  
}
    
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplatePostinstall;
