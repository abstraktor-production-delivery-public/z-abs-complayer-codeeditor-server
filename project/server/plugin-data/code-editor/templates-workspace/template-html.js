
'use strict';

const Os = require('os');


class TemplateHtml {
  static create(lowercaseAppName, description) {
    const text = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="${description}">
    <script src="/abs-scripts/3ppLayer-D3-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/3ppLayer-jQuery-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/3ppLayer-Nodejs-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/3ppLayer-React-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/3ppLayer-Reactdom-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/CoreLayer-cs.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/CoreLayer-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/CompLayer-Router-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/CompLayer-Bootstrap-client.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/AppLayer-cs.js" type="text/javascript" defer></script>
    <script src="/abs-scripts/AppLayer-client.js" type="text/javascript" defer></script>
    <link rel="preload" href="/abs-css/bundle-${lowercaseAppName}.css" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" media="screen">
    <noscript><link rel="stylesheet" href="/abs-css/bundle-${lowercaseAppName}.css"></noscript>
    <link rel="preload" href="/abs-css/bundle-CompLayer-Bootstrap.css" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" media="screen">
    <noscript><link rel="stylesheet" href="/abs-css/bundle-CompLayer-Bootstrap.css"></noscript>
    <link rel="preload" href="/abs-css/bundle-CoreLayer.css" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" media="screen">
    <noscript><link rel="stylesheet" href="/abs-css/bundle-CoreLayer.css"></noscript>
  </head>
  <body>
    <div id="app"></div>
  </body>
</html>


`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateHtml;
