
'use strict';

const Os = require('os');


class TemplateSettings {
  static create(lowercaseAppName) {
const text = `
{
  "name": "${lowercaseAppName}",
  "type": "local-server",
  "defaultSite": "start",
  "release-steps": [
    {
      "release-step": "source",
      "httpServers": [
        {
          "name": "http-server",
          "type": "web & rest",
          "host": "127.0.0.1",
          "port": 9095,
          "testHost": "127.0.0.1",
          "testPort": 9195,
          "npmHost": "127.0.0.1",
          "npmPort": 9295,
          "directories": ["../Documentation"]
        }
      ],
      "wsWebServers": [
        {
          "name": "ws-web-server",
          "type": "rest",
          "host":  "127.0.0.1",
          "port": 9096,
          "testHost":  "127.0.0.1",
          "testPort": 9196,
          "npmHost":  "127.0.0.1",
          "npmPort": 9296,
          "protocols": ["z-plugin-protocol-server-actorjs-cs"]
        }
      ],
      "tcpClients": [
        {
          "name": "persistant-node-proxy",
          "dependency": true,
          "type": "ip",
          "host": "127.0.0.1",
          "port": 9014,
          "testHost": "127.0.0.1",
          "testPort": 9114,
          "npmHost": "127.0.0.1",
          "npmPort": 9214,
          "protocols": ["z-plugin-protocol-client-user-client-cs"]
        }
      ]
	  }
  ],
  "pluginDatas": [],
  "pluginServices": [],
  "pluginComponents": [],
  "plugins": []
}

`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateSettings;
