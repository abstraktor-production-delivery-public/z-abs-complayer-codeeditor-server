
'use strict';

const Os = require('os');


class TemplateGitignore {
  static create() {
    const text = `
build
dist
temp
node_modules
actorjs.sh
actorjs.bat
actorjs-cmd.bat
aj.bat
postinstall.log
.DS_Store
actorjs.arg
    
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateGitignore;
