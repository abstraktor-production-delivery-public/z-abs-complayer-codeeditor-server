
'use strict';

const Os = require('os');


class TemplateBat {
  static create() {
    const text = `

@echo off
title actorjs
node .\\main.js %* --silent 
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateBat;
