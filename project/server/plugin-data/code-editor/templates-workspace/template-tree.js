
'use strict';

const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const Os = require('os');


class TemplateTree {
  static create() {
    const projectFolderGuid = GuidGenerator.create();
    const clientFolderGuid = GuidGenerator.create();
    const clientMainJsxGuid = GuidGenerator.create();
    const actionsFolderGuid = GuidGenerator.create();
    const componentsFolderGuid = GuidGenerator.create();
    const componentsSiteJsxGuid = GuidGenerator.create();
    const cssFolderGuid = GuidGenerator.create();
    const htmlFolderGuid = GuidGenerator.create();
    const htmlIndexGuid = GuidGenerator.create();
    const storesFolderGuid = GuidGenerator.create();
    const serverFolderGuid = GuidGenerator.create();
    const pluginDataFolderGuid = GuidGenerator.create();
    const pluginServiceFolderGuid = GuidGenerator.create();
    const text = `\
${projectFolderGuid};;project;project_folder;.;[actorjs,js,jsx,html]
${clientFolderGuid};${projectFolderGuid};client;static_folder;./project;[js,jsx,html]
${clientMainJsxGuid};${clientFolderGuid};main.jsx;file;./project/client;jsx
${actionsFolderGuid};${clientFolderGuid};actions;folder;./project/client;[js]
${componentsFolderGuid};${clientFolderGuid};components;folder;./project/client;[js]
${componentsSiteJsxGuid};${componentsFolderGuid};site.jsx;file;./project/client/components;jsx
${cssFolderGuid};${clientFolderGuid};css;folder;./project/client;[js]
${htmlFolderGuid};${clientFolderGuid};html;folder;./project/client;[js]
${htmlIndexGuid};${htmlFolderGuid};index.html;file;./project/client/html;html
${storesFolderGuid};${clientFolderGuid};stores;folder;./project/client;[js]
${serverFolderGuid};${projectFolderGuid};server;static_folder;./project;[js,jsx,html]
${pluginDataFolderGuid};${serverFolderGuid};plugin-data;folder;./project/client;[js]
${pluginServiceFolderGuid};${serverFolderGuid};plugin-service;folder;./project/client;[js]
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateTree;
