
'use strict';

const Os = require('os');


class TemplateSiteJsx {
  static create(appName, lowercaseAppName) {
    const text = `
'use strict';

//import YourComponent from './your-component/your-component';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';

 
export default class Middle extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.serviceExistsDocs = false;
  }
    
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle_outer">
        {/*<Route switch="/your-component*" handler={YourComponent} />*/}
      </div>
    );
  }
}


`.replaceAll('\n', Os.EOL);
    return text;
  }
}



module.exports = TemplateSiteJsx;
