
'use strict';

const Os = require('os');


class TemplateMainJsx {
  static create(appName, lowercaseAppName) {
    const text = `

'use strict';


import Site from './components/site';
import Router from 'z-abs-complayer-router-client/client/react-component/router';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';
import { createRoot } from 'react-dom/client';


class Actor extends ReactComponentBase {
  constructor(props) {
    super(props);
    Actor.routerRef = React.createRef();
    window.abstractorReleaseData = {
      appName: '${lowercaseAppName}',
      appTitle: '${appName}'
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="main">
        <Router ref={Actor.routerRef}>
          <Site />
        </Router>
      </div>
    );
  }
}

async function abstractorRerender(uri) {
  return new Promise((resolve, reject) => {
    if(null !== Actor.routerRef.current) {
      Actor.routerRef.current.rerender(uri, () => {
        resolve();
      });
    }
    else {
      resolve();
    }
  });	
}

window.abstractorRerender = abstractorRerender;
Actor.routerRef = null;


const container = document.getElementById('app');
const root = createRoot(container);
root.render(<Actor />);


`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateMainJsx;
