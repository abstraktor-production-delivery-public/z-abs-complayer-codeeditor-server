
'use strict';

const Os = require('os');


class TemplateMain {
  static create(lowercaseAppName) {
    const text = `
'use strict';

const Path = require('path');


try {
  require(Path.resolve('../z-build-require/project/server/module')).init(true, '');
  const AbsProcess = require('z-build-project/project/server/process');
  AbsProcess.run('${lowercaseAppName}', 'source', '');
}
catch(err) {
  console.log('Could not start ${lowercaseAppName}.', err);
}
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateMain;
