
'use strict';

const Os = require('os');


class TemplateReadme {
  static create() {
    const text = `
# README #

Abstraktor AB
`.replaceAll('\n', Os.EOL);
    return text;
  }
}


module.exports = TemplateReadme;
