
'use strict';

const Os = require('os');


class TemplatePackageJson {
  static create(isApp, appName, lowercaseAppName) {
    const shortName = appName.replace(/[^A-Z]+/g, "").toLowerCase();
    if(isApp) {
      const appJson = {
        name: `${lowercaseAppName}`,
        version: "0.0.0-aj-alpha.0+production-0",
        shortname: `${shortName}`,
        longname: `${lowercaseAppName}`,
        folder: `${lowercaseAppName}`,
        description: `${lowercaseAppName} - .`,
        scripts: {
          postinstall: "node -e \"try{require('./npm-post-install.js')}catch(e){console.log(e);}\""
        },
        author: "Abstraktor AB",
        license: "SEE LICENSE IN License agreement ActorJs.pdf",
        browser: {
          jquery: "./node_modules/jquery/dist/jquery.js",
        },
        dependencies: {},
        repository: `https://gitlab.com/abstraktor-development-apps/${lowercaseAppName}.git`
      };
      appJson["release-step"] = "development";
      appJson.dependencies["@babel/plugin-transform-runtime"] = "7.24.7";
      appJson.dependencies["@babel/preset-env"] = "7.25.3";
      appJson.dependencies["@babel/preset-react"] = "7.24.7";
      appJson.dependencies["@babel/runtime"] = "7.25.0";
      appJson.dependencies.bufferutil = "4.0.8";
      appJson.dependencies["cli-color"] = "2.0.4";
      appJson.dependencies["@codemirror/view"] = "6.30.0";
      appJson.dependencies["@codemirror/state"] = "6.4.1";
      appJson.dependencies["@codemirror/commands"] = "6.6.0";
      appJson.dependencies["@codemirror/autocomplete"] = "6.18.0";
      appJson.dependencies["@codemirror/search"] = "6.5.6";
      appJson.dependencies["@codemirror/language"] = "6.10.2";
      appJson.dependencies["@codemirror/lang-javascript"] = "6.2.2";
      appJson.dependencies["@codemirror/lang-css"] = "6.2.1";
      appJson.dependencies["@codemirror/lang-html"] = "6.4.9";
      appJson.dependencies["@codemirror/lang-json"] = "6.0.1";
      appJson.dependencies["@codemirror/lang-markdown"] = "6.2.5";
      appJson.dependencies["@uiw/codemirror-theme-vscode"] = "4.23.0";
      appJson.dependencies.jquery = "3.7.1";
      appJson.dependencies["mime-types"] = "2.1.35";
      appJson.dependencies.react = "18.3.1";
      appJson.dependencies["react-dom"] = "18.3.1";
      appJson.dependencies["simple-git"] = "3.25.0";
      appJson.dependencies["sudo-js"] = "1.0.2";
      appJson.dependencies["sudo-prompt"] = "9.2.1";
      appJson.dependencies.ws ="8.18.0";
      return appJson;
    }
    else {
      const nodeJson = {
        name: `${lowercaseAppName}`,
        version: "0.0.0-aj-alpha.0+production-0",
        shortname: `${shortName}`,
        longname: `${lowercaseAppName}`,
        folder: `${lowercaseAppName}`,
        description: `${lowercaseAppName} - .`,
        scripts: {
          postinstall: "node -e \"try{require('./npm-post-install.js')}catch(e){console.log(e);}\""
        },
        author: "Abstraktor AB",
        license: "SEE LICENSE IN License agreement ActorJs.pdf",
        dependencies: {},
        repository: `https://gitlab.com/abstraktor-development-nodes/${lowercaseAppName}.git`
      };
      nodeJson["release-step"] = "development";
      nodeJson.dependencies["@babel/preset-env"] = "7.25.3";
      nodeJson.dependencies["@babel/runtime"] = "7.25.0";
      nodeJson.dependencies.bufferutil = "4.0.8";
      nodeJson.dependencies["cli-color"] = "2.0.4";
      nodeJson.dependencies["mime-types"] = "2.1.35";
      nodeJson.dependencies["simple-git"] = "3.25.0";
      nodeJson.dependencies["sudo-js"] = "1.0.2";
      nodeJson.dependencies["sudo-prompt"] = "9.2.1";
      nodeJson.dependencies.ws = "8.18.0";
      return nodeJson;
    }
  }
}


module.exports = TemplatePackageJson;
