
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const ActorPathBuild = require('z-abs-corelayer-server/server/path/actor-path-build');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorFileDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(file, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
    const type = ActorPathProject.getCacheName(file, projectType);
    if(ActorPathProject.serverJs === type) {
      this.asynchRmFileResponse(ActorPathDist.getCodeFile(file, projectType));
    }
    else if(ActorPathProject.clientServerJs === type) {
      this.asynchRmFileResponse(ActorPathDist.getCodeFile(file, projectType));
      this.asynchRmFileResponse(ActorPathBuild.getCodeFile(file, projectType));
    }
    else if(ActorPathProject.clientJs === type) {
      this.asynchRmFileResponse(ActorPathBuild.getCodeFile(file, projectType));
    }
    else if(ActorPathProject.clientJsx === type) {
      this.asynchRmFileResponse(ActorPathBuild.getCodeFile(file.substring(0, file.length - '.jsx'.length) + '.js'), projectType);
    }
    else if(ActorPathProject.apiJs === type) {
      this.asynchRmFileResponse(ActorPathDist.getCodeFile(file, projectType));
    }
    this.asynchRmFileResponse(ActorPathProject.getFile(file, projectType, isExternalPlugin, workspaceName));
  }
}


module.exports = CodeEditorFileDelete;
