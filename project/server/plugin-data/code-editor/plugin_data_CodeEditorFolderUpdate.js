
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorFolderUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(oldPath, newPath, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
    this.asynchRenameResponse(ActorPathProject.getFile(oldPath, projectType, isExternalPlugin, workspaceName), ActorPathProject.getFile(newPath, projectType, isExternalPlugin, workspaceName));
  }
}


module.exports = CodeEditorFolderUpdate;
