
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorFileUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(file, content, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
    this.asynchWriteTextFileResponse(ActorPathProject.getFile(file, projectType, isExternalPlugin, workspaceName), content);
  }
}

module.exports = CodeEditorFileUpdate;
