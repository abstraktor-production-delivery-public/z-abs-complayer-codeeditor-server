
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorFolderDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(file, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
   this.asynchRmdirResponse(ActorPathProject.getFile(file, projectType, isExternalPlugin, workspaceName), true);
  }
}


module.exports = CodeEditorFolderDelete;
