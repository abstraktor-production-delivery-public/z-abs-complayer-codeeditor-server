
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class CodeEditorFileRename extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(oldPath, newPath, projectType, plugin, workspaceName) {
    const isExternalPlugin = this.hasOrganization && !!plugin;
    Fs.access(ActorPathProject.getFile(newPath, projectType, isExternalPlugin, workspaceName), Fs.F_OK, (err) => {
      if(err && 'ENOENT' === err.code) {
        this.asynchRenameResponse(ActorPathProject.getFile(oldPath, projectType, isExternalPlugin, workspaceName), ActorPathProject.getFile(newPath, projectType, isExternalPlugin, workspaceName));
      }
      else {
        this.expectAsynchResponseError(`File '${newPath}' does already exist.`);
      }
    });
  }
}


module.exports = CodeEditorFileRename;
