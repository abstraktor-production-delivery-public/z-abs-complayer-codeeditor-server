
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class CodeEditorProjectToggle extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(content, name, type) {
    this.asynchWriteFileResponse(ActorPathGenerated.getProjectFile(name, type), content, false);
  }
}

module.exports = CodeEditorProjectToggle;
