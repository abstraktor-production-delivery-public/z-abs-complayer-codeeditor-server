
'use strict';

const Project = require('z-abs-corelayer-cs/clientServer/project');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ActorPathProject = require('z-abs-corelayer-server/server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Os = require('os');


class CodeEditorWorkspaceGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(appName, workspaceName) {
    if(appName) {
      this._handleWorkspace(appName, workspaceName ? workspaceName : appName);
    }
    else {
      this.asynchReadFile(ActorPathGenerated.getWorkspaceRecentFiles(), (err, data) => {
        if(err) {
          return this.expectAsynchResponseError(`Could not get recent project files '${ActorPathGenerated.getWorkspaceRecentFiles()}'.`);
        }
        else {
          const lastWorkspace = data.recentWorkspaces[0];
          this._handleWorkspace(lastWorkspace.appName, lastWorkspace.workspaceName);
        }
      });
    }
  }
  
  _handleWorkspace(appName, workspaceName) {
    const workspaceFile = ActorPathProject.getWorkspaceFile(appName, workspaceName);
    this.asynchReadFile(workspaceFile, (err, workspace) => {
      if(err) {
        ddb.error(err);
      }
      else {
        let projectPendings = {p:0};
        let projects = [];
        let projectSource = null;
        let pendings = 2;
        this._readAppProject(appName, null, (err, _projectSource) => {
          projectSource = _projectSource;
          if(0 === --pendings) {
            this.expectAsynchResponseSuccess(projectSource);
            this.expectAsynchResponseSuccess(projects);
            this.expectAsynchResponseSuccess(Os.EOL);
          }
        });
        workspace.projects.forEach((project) => {
          this._readProject(project, projects, projectPendings, () => {
            if(0 === --pendings) {
              this.expectAsynchResponseSuccess(projectSource);
              this.expectAsynchResponseSuccess(projects);
              this.expectAsynchResponseSuccess(Os.EOL);
            }
          });
        });
      }
    });
  }
  
  _readAppProject(name, type, cb) {
    let pendings = 2;
    let pureProject = null;
    let jsonProject = null;
    const codeProjectPureFileName = ActorPathProject.getCodeProjectPureFile(name, type, false);
    this.asynchReadTextFile(codeProjectPureFileName, (err, pureP) => {
      if(err) {
        ddb.error(`Could not read '${codeProjectPureFileName}'`, err);
      }
      else {
        pureProject = pureP;
      }
      if(0 === --pendings) {
        this._handleAppProject(pureProject, jsonProject, name, type, cb);
      }
    });
    this.asynchReadFile(ActorPathGenerated.getProjectFile(name, type), (err, jsonP) => {
      if(!err) {
        jsonProject = jsonP;
      }
      if(0 === --pendings) {
        this._handleAppProject(pureProject, jsonProject, name, type, cb);
      }
    });
  }
  
  _handleAppProject(pureProject, jsonProject, name, type, cb) {
    const project = new Project();
    project.importPure(pureProject, jsonProject);
    this.asynchWriteFile(ActorPathGenerated.getProjectFile(name, type), project.source, (err, data) => {
      cb(err, data);
    });
  }
    
  _readProject(project, projects, projectPendings, cb) {
    ++projectPendings.p;
    let pendings = 2;
    let pureProject = null;
    let jsonProject = null;
    const isExternalPlugin = this.hasOrganization && !!project.plugin;
    const codeProjectPureFileName = ActorPathProject.getCodeProjectPureFile(project.name, project.type, isExternalPlugin);
    const codeProjectFileName = ActorPathGenerated.getProjectFile(project.name, project.type);
    this.asynchReadTextFile(codeProjectPureFileName, (err, pureP) => {
      if(err) {
        ddb.error(`Could not read '${codeProjectPureFileName}'`, err);
      }
      else {
        pureProject = pureP;
      }
      if(0 === --pendings) {
        this._handleProject(pureProject, jsonProject, codeProjectFileName, project.type, isExternalPlugin, (err, data) => {
          projects.push(data);
          if(0 === --projectPendings.p) {
            cb();
          }
        });
      }
    });
    
    this.asynchReadFile(codeProjectFileName, (err, jsonP) => {
      if(!err) {
        jsonProject = jsonP;
      }
      if(0 === --pendings) {
        this._handleProject(pureProject, jsonProject, codeProjectFileName, project.type, isExternalPlugin, (err, data) => {
          projects.push(data);
          if(0 === --projectPendings.p) {
            cb();
          }
        });
      }
    });
  }
  
  _handleProject(pureProject, jsonProject, codeProjectFileName, type, isExternalPlugin, cb) {
    const project = new Project([], type);
    project.importPure(pureProject, jsonProject);
    if(1 <= project.source.length) {
      this.asynchWriteFile(codeProjectFileName, project.source, (err, jsonP) => {
        if(!err) {
          cb(null, {
            type: type,
            plugin: isExternalPlugin,
            source: jsonP
          });
        }
        else {
          cb(err, null);
          ddb.error(`Could not write '${codeProjectFileName}'`, err);
          this.expectAsynchResponseError(`Could not write file '${codeProjectFileName}'.`);
        }
      });
    }
    else {
      cb(null, {
        type: type,
        plugin: isExternalPlugin,
        source: [{title: name}]
      });
    }
  }
}


module.exports = CodeEditorWorkspaceGet;
