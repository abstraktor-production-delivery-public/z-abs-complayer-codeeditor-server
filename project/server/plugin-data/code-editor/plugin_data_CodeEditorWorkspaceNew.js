
'use strict';

const TemplateBat = require('./templates-workspace/template-bat');
const TemplateGitattributes = require('./templates-workspace/template-gitattributes');
const TemplateGitignore = require('./templates-workspace/template-gitignore');
const TemplateHelpText = require('./templates-workspace/template-help-text');
const TemplateHtml = require('./templates-workspace/template-html');
const TemplateMain = require('./templates-workspace/template-main');
const TemplateMainJsx = require('./templates-workspace/template-main-jsx');
const TemplatePackageJson = require('./templates-workspace/template-package-json');
const TemplatePostinstall = require('./templates-workspace/template-postinstall');
const TemplateReadme = require('./templates-workspace/template-readme');
const TemplateSiteJsx = require('./templates-workspace/template-site-jsx');
const TemplateSettings = require('./templates-workspace/template-settings');
const TemplateTree = require('./templates-workspace/template-tree');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require ('path');


class CodeEditorWorkspaceNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.appName = '';
    this.lowercaseAppName = '';
    this.noDashAppName = '';
    this.lowecaseNoDashAppName = '';
  }
  
  onRequest(appName, appType) {
    const workspace = {
      settings: {},
      projects: []
    };
    this.appName = appName;
    this.lowercaseAppName = appName.toLowerCase();
    this.noDashAppName = appName.split('-').join('');
    this.lowecaseNoDashAppName = this.noDashAppName.toLowerCase();
    const shortName = appName.replace(/[^A-Z]+/g, "").toLowerCase();
    this.isApp = this.lowercaseAppName.startsWith('actor');
    const appPath = Path.normalize(`${ActorPath.getActorPath()}..${Path.sep}${this.lowercaseAppName}`);
    const folder = `${ActorPath.getActorParentPath()}${Path.sep}${this.lowercaseAppName}${Path.sep}workspace`;
    this.asynchMkdir(folder, (err) => {
      const type = 0 === appType ? "clientServer" : "server";
      if(err) {
        this.expectAsynchResponseError(`Could not create floder: '${folder}'`);
      }
      else {
        const newWorkspace = {
          name: appName,
          type: type,
          silent: false,
          projects: [],
          builds: [],
          tasks: []
        };
        const builds = [];
        const tasks = [];
        this._createWorkspace(newWorkspace, tasks, builds, type);
        
        let pendings = {p:0};
        this._createAllDirectories(appPath, type, (err) => {
          this._createTaskFiles(tasks, folder, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createBuildFiles(builds, folder, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createWorkspaceFile(newWorkspace, folder, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateHelpText.create(this.lowercaseAppName, this.noDashAppName), `${folder}${Path.sep}simple-help.txt`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateGitattributes.create(), `${appPath}${Path.sep}.gitattributes`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateGitignore.create(), `${appPath}${Path.sep}.gitignore`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile('', `${appPath}${Path.sep}CHANGELOG.md`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateReadme.create(), `${appPath}${Path.sep}README.md`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplatePostinstall.create(), `${appPath}${Path.sep}npm-post-install.js`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateTree.create(), `${appPath}${Path.sep}project${Path.sep}${this.lowercaseAppName}-project.tree`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateMain.create(this.lowercaseAppName), `${appPath}${Path.sep}main.js`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateBat.create(), `${appPath}${Path.sep}${shortName}.bat`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          this._createTextFile(TemplateSettings.create(this.lowercaseAppName), `${appPath}${Path.sep}settings.json`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
          if('clientServer' === type) {
            this._createTextFile(TemplateMainJsx.create(this.appName, this.lowercaseAppName), `${appPath}${Path.sep}project${Path.sep}client${Path.sep}main.jsx`, pendings, (err) => {
              if(err) {console.log(err);}
              if(0 === --pendings.p) {
                this.expectAsynchResponseSuccess('OK');
              }
            });
            this._createTextFile(TemplateSiteJsx.create(), `${appPath}${Path.sep}project${Path.sep}client${Path.sep}components${Path.sep}site.jsx`, pendings, (err) => {
              if(err) {console.log(err);}
              if(0 === --pendings.p) {
                this.expectAsynchResponseSuccess('OK');
              }
            });
            this._createTextFile(TemplateHtml.create(this.lowercaseAppName, ''), `${appPath}${Path.sep}project${Path.sep}client${Path.sep}html${Path.sep}index.html`, pendings, (err) => {
              if(err) {console.log(err);}
              if(0 === --pendings.p) {
                this.expectAsynchResponseSuccess('OK');
              }
            });
          }
          this._createFile(TemplatePackageJson.create(this.isApp, this.appName, this.lowercaseAppName), `${appPath}${Path.sep}package.json`, pendings, (err) => {
            if(err) {console.log(err);}
            if(0 === --pendings.p) {
              this.expectAsynchResponseSuccess('OK');
            }
          });
        });
      }
    }, true);
  }
  
  _createWorkspace(newWorkspace, tasks, builds, type) {
    newWorkspace.projects.push(this._createProject("z-build-build", "server"));
    newWorkspace.projects.push(this._createProject("z-build-project", "server"));
    newWorkspace.projects.push(this._createProject("z-build-require", "server"));
    newWorkspace.projects.push(this._createProject("z-abs-corelayer-server", "server"));
    
    newWorkspace.projects.push(this._createProject("z-abs-corelayer-cs", "clientServer"));
    
    this._createBuildServer(newWorkspace.builds, builds, "Server:AppLayer/cs", "Server-AppLayer-cs", "build-server-js", null, ".");
    this._createBuildServer(newWorkspace.builds, builds, "Server:AppLayer/server", "Server-AppLayer-server", "build-server-js", null, ".");
    
    if('clientServer' === type) {
      newWorkspace.projects.push(this._createProject("z-abs-3pplayer-d3-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-3pplayer-jquery-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-3pplayer-nodejs-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-3pplayer-react-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-3pplayer-reactdom-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-corelayer-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-complayer-router-client", "client"));
      newWorkspace.projects.push(this._createProject("z-abs-complayer-bootstrap-client", "client"));
      
      this._createBuildClient(newWorkspace.builds, builds, "Client:AppLayer/cs", "Client-AppLayer-cs", "build-client-js", null, ".", "cs");
      this._createBuildClient(newWorkspace.builds, builds, "Client:AppLayer/client", "Client-AppLayer-client", "build-client-js", null, ".", "client");
      this._createBuildClient(newWorkspace.builds, builds, "Client:AppLayer/client-jsx", "Client-AppLayer-client-jsx", "build-client-jsx", null, ".", "client");
      
      this._createBundleClient(newWorkspace.builds, builds, "Bundle:AppLayer/cs", "Bundle-AppLayer-cs", "build-client-bundle", null, ".", "cs",
        ["react", "react-dom", "jquery", "bootstrap", "os"],
        ["./build/z-abs-corelayer-cs/**/*.js", "./build/z-abs-3pplayer-nodejs-client/**/*.js", "./build/z-abs-3pplayer-jquery-client/**/*.js", "./build/z-abs-3pplayer-react-client/**/*.js", "./build/z-abs-3pplayer-reactdom-client/**/*.js", "./build/z-abs-3pplayer-d3-client/**/*.js"],
        null,
        true,
        ["Client:AppLayer/cs", "Client:CoreLayer/cs", "Client:3ppLayer/Nodejs-client", "Client:3ppLayer/jQuery-client", "Client:3ppLayer/React-client", "Client:3ppLayer/Reactdom-client", "Client:3ppLayer/D3-client"]
      );
      this._createBundleClient(newWorkspace.builds, builds, "Bundle:AppLayer/client", "Bundle-AppLayer-client", "build-client-bundle", null, ".", "client",
        ["react", "react-dom", "jquery", "bootstrap", "os"],
        ["./build/AppLayer/cs/**/*.js", "./build/z-abs-complayer-router-client/**/*.js", "./build/z-abs-complayer-bootstrap-client/**/*.js", "./build/z-abs-corelayer-client/**/*.js", "./build/z-abs-corelayer-cs/**/*.js", "./build/z-abs-3pplayer-nodejs-client/**/*.js", "./build/z-abs-3pplayer-jquery-client/**/*.js", "./build/z-abs-3pplayer-react-client/**/*.js", "./build/z-abs-3pplayer-reactdom-client/**/*.js", "./build/z-abs-3pplayer-d3-client/**/*.js"],
        null,
        false,
        ["Client:AppLayer/client", "Client:AppLayer/client-jsx", "Client:AppLayer/cs", "Client:CompLayer/Router-client-jsx", "Client:CompLayer/Bootstrap-client-jsx", "Client:CompLayer/Bootstrap-client", "Client:CoreLayer/client", "Client:CoreLayer/client-jsx", "Client:CoreLayer/cs", "Client:3ppLayer/Nodejs-client", "Client:3ppLayer/jQuery-client", "Client:3ppLayer/React-client", "Client:3ppLayer/Reactdom-client", "Client:3ppLayer/D3-client"]
      );
      
      this._createBundleCss(newWorkspace.builds, builds, `client-css-${this.lowercaseAppName}-bundle`, `client-css-${this.lowercaseAppName}-bundle`, "build-client-rc-bundle", null, ".", {path: './project/client/css', filters: ['css']}, []);
      this._createBundleResource(newWorkspace.builds, builds, "server-html-rc", "server-html-rc", "build-server-rc", null, ".", "html", {path: './project/client/html', filters: ['html']}, "./dist/html", []);
      this._createBundleResource(newWorkspace.builds, builds, `server-image-${this.lowercaseAppName}-rc`, `server-image-${this.lowercaseAppName}-rc`, "build-server-rc",  "actorjs-documentation-bin", "Documentation", `image-${this.lowercaseAppName}`,  {path: './images', filters: ['svg', 'ico', 'gif', 'jpg', 'png']}, "./dist/images", []);
    }
    
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:build", "clean-build", "task-clean", ["./build", "../Generated/Build"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:dist",  "clean-dist", "task-clean", ["./dist"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:temp", "clean-temp", "task-clean", ["./temp"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:generated", "clean-generated", "task-clean", ["../Generated"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:node", "clean-node", "task-clean", ["node_modules"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:compiled",  "clean-compiled", "task-clean", ["./build", "./dist", "../Generated/Build"], true);
    this._createTaskClean(newWorkspace.tasks, tasks, "clean:all", "clean-all", "task-clean", ["./build", "./dist", "./temp", "../Generated", "node_modules"], true);
    this._createTasksimple(newWorkspace.tasks, tasks, "simple:help", "simple-help", "task-simple", "\r\n console.log(`${text}`);\r\ncb();\r\n", ["./workspace/simple-help.txt"]);
    this._createTaskList(newWorkspace.tasks, tasks, "list:debug", "list-debug", "task-list", ["task:env-development:solution", `local-server-${this.lowercaseAppName}`]);
    this._createTaskList(newWorkspace.tasks, tasks, "list:release", "list-release", "task-list", ["task:env-production:solution", `local-server-${this.lowercaseAppName}`]);
    this._createTaskList(newWorkspace.tasks, tasks, "list:redebug", "list-redebug", "task-list", ["task:clean:compiled", "task:env-development:solution", `local-server-${this.lowercaseAppName}`]);
    this._createTaskList(newWorkspace.tasks, tasks, "list:rerelease", "list-rerelease", "task-list", ["task:clean:compiled", "task:env-production:solution", `local-server-${this.lowercaseAppName}`]);
    this._createTaskList(newWorkspace.tasks, tasks, "list:start", "list-start", "task-list", [`local-server-${this.lowercaseAppName}`, "task:watch"]);
    this._createTaskList(newWorkspace.tasks, tasks, "default", "list-default",  "task-list", ["task:start"]);
  }
  
  _createAllDirectories(appPath, type, cb) {
    let pendings = 1;
    if('clientServer' === type) {
      ++pendings;
      this._createDirectories(`${appPath}${Path.sep}project${Path.sep}client${Path.sep}`, ['actions', 'components', 'css', 'html', 'stores'], (err) => {
        if(err) {console.log(err);}
        if(0 === --pendings) {
          cb(err);
        }
      });
    }
    this._createDirectories(`${appPath}${Path.sep}project${Path.sep}server${Path.sep}`, ['plugin-data', 'plugin-service'], (err) => {
      if(err) {console.log(err);}
      if(0 === --pendings) {
        cb(err);
      }
    });
  }
  
  _createDirectories(path, directories, cb) {
    let pendings = directories.length;
    directories.forEach((directory) => {
      this.asynchMkdir(`${path}${Path.sep}${directory}`, (err) => {
        if(0 === --pendings) {
          cb();
        }
      }, true);
    });
  }
  
  _createTaskFiles(tasks, folder, externalPendings, cb) {
    ++externalPendings.p;
    let pendings = tasks.length;
    tasks.forEach((task) => {
      this.asynchWriteFile(`${folder}${Path.sep}${task.fileName}.tsk`, task.data, (err) => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  _createBuildFiles(builds, folder, externalPendings, cb) {
    ++externalPendings.p;
    let pendings = builds.length;
    builds.forEach((build) => {
      this.asynchWriteFile(`${folder}${Path.sep}${build.fileName}.bld`, build.data, (err) => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  _createWorkspaceFile(newWorkspace, folder, externalPendings, cb) {
    ++externalPendings.p;
    this.asynchWriteFile(`${folder}${Path.sep}${this.lowercaseAppName}.wrk`, newWorkspace, cb);
  }
  
  _createTextFile(data, file, externalPendings, cb) {
    ++externalPendings.p;
    this.asynchWriteTextFile(file, data, cb);
  }
  
  _createFile(data, file, externalPendings, cb) {
    ++externalPendings.p;
    this.asynchWriteFile(file, data, cb);
  }
  
  _createProject(name, type) {
    return {
      name,
      type,
      new: true
    };
  }
  
  _createBuildServer(wrkBuilds, builds, name, fileName, requireName, repo, repoPath) {
    wrkBuilds.push({
      name,
      fileName,
      requireName,
      repo,
      repoPath
    });
    builds.push({
      fileName,
      data: {
        layer: "App",
        part: "server",
        source: {path: './project/server', filters: ['js']},
        dest: "./dist/Layers/AppLayer/server",
        replaceCondition: "(LOG_ENGINE|LOG_ERROR|LOG_DEBUG)\\(.*\\);",
        replaceHandle: "\r\n  const logParametersText = match.substring(match.indexOf('(') + 1, match.lastIndexOf(')'));\r\n  const logParameters = logParametersText.split(',');\r\n  if('LOG_ENGINE' === parameters[0]) {\r\n    return `if(Logger.isLogEngine(${logParameters[0].trim()})) {Logger.engine(${logParameters}, __filename);}`;\r\n  }\r\n  else if('LOG_ERROR' === parameters[0]) {\r\n    return `if(Logger.isLogError(${logParameters[0].trim()})) {Logger.error(${logParameters}, __filename);}`;\r\n  }\r\n  else if('LOG_DEBUG' === parameters[0]) {\r\n    return `if(Logger.isLogDebug(${logParameters[0].trim()})) {Logger.debug(${logParameters}, __filename);}`;\r\n  }\r\n",
        dependencyNames: []
      }
    });
  }
  
  _createBuildClient(wrkBuilds, builds, name, fileName, requireName, repo, repoPath, part) {
    wrkBuilds.push({
      name,
      fileName,
      requireName,
      repo,
      repoPath
    });
    const extention = 'build-client-jsx' === requireName ? 'jsx' : 'js'
    const source = "client" === part ? {path: './project/client', filters: [extention]} : {path: './project/clientServer', filters: [extention]}
    const dest = "client" === part ? "./build/AppLayer/client" :"./build/AppLayer/clientServer";
    builds.push({
      fileName,
      data: {
        layer: "App",
        part: part,
        source: source,
        dest,
        dependencyNames: []
      }
    });
  }
  
  _createBundleClient(wrkBuilds, builds, name, fileName, requireName, repo, repoPath, part, externals, externalGlobs, exports, exportSource, dependencyNames) {
    wrkBuilds.push({
      name,
      fileName,
      requireName,
      repo,
      repoPath
    });
    const source = "client" === part ? {path: './build/AppLayer/client', filters: ['js']} : {path: './build/AppLayer/clientServer', filters: ['js']}
    builds.push({
      fileName,
      data: {
        layer: "App",
        part,
        source: source,
        dest: "./dist/scripts",
        _bundle: true,
        ignores: null,
        externals,
        externalGlobs,
        exports,
        exportSource,
        dependencyNames
      }
    });
  }
  
  _createBundleCss(wrkBuilds, builds, name, fileName, requireName, repo, repoPath, source, dependencyNames) {
    wrkBuilds.push({
      name,
      fileName,
      requireName,
      repo,
      repoPath
    });
    builds.push({
      fileName,
      data: {
        part: this.lowercaseAppName,
        source: source,
        dest: "./dist/css",
        _bundle: true,
        dependencyNames
      }
    });
  }
  
  _createBundleResource(wrkBuilds, builds, name, fileName, requireName, repo, repoPath, part, source, dest, dependencyNames) {
    wrkBuilds.push({
      name,
      fileName,
      requireName,
      repo,
      repoPath
    });
    builds.push({
      fileName,
      data: {
        part,
        source: source,
        dest,
        dependencyNames
      }
    });
  }
  
  _createTaskClean(wrkTasks, tasks, name, fileName, requireName, paths, reqursive) {
    wrkTasks.push(this._createTask(name, fileName, requireName));
    tasks.push({
      fileName,
      data: {
        part: name,
        paths,
        reqursive
      }
    });
  }
  
  _createTaskList(wrkTasks, tasks, name, fileName, requireName, list) {
    const part = name.substring(5);
    wrkTasks.push(this._createTask(name, fileName, requireName));
    tasks.push({
      fileName,
      data: {
        part,
        list
      }
    });
  }
  
  _createTasksimple(wrkTasks, tasks, name, fileName, requireName, handle, parameters) {
    wrkTasks.push(this._createTask(name, fileName, requireName));
    tasks.push({
      fileName,
      data: {
        part: name,
        handle,
        parameters
      }
    });
  }
  
  _createTask(name, fileName, requireName) {
    return {
      name,
      fileName,
      requireName
    };
  }
}


module.exports = CodeEditorWorkspaceNew;
